# cgi auth client

## Activação em projecto jhipster

### Configuração spring security

* Package **.config
    Alterar a classe SecurityConfig
    Substituir método securityConfigurerAdapter() por:
    ```
    private SecurityConfigurerAdapter securityConfigurerAdapter() {
        	//return new JWTConfigurer(tokenProvider); silvagc alterei aqui
        	return authConfig;
    	}
    ```
    
    Injectar AuthClientConfigurer authConfig;
    
* Application
    Adicionar o package pt.cgi.security.authclient aos packages  consultados pelo spring. 
    Poderão fazer isso usando @ComponentScan. 
    Exemplo:
    ```
    @ComponentScan(basePackages = {"com.cgi.portalams", "pt.cgi.security.authclient"})
    ```

* Nota
    O principal da autenticação será uma implementação da classe UserDetails
    o método getUsername devolverá uma string, que corresponde ao login
    Qualquer propriedade que desejem reescrever deverá ser definida no vosso ficheiro de properties/yaml ( dentro do vosso projeto ) e não no application.yaml do cgi-auth-client