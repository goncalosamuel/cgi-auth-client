package pt.cgi.security.authclient.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.cgi.security.authclient.AuthConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * REST controller for managing the current user's authentication.
 */
@RestController
@RequestMapping("/api")
public class AuthClientController {

    private final AuthConfig config;

    private final Logger LOG = LoggerFactory.getLogger(AuthClientController.class);

    public AuthClientController(AuthConfig config) {
        this.config = config;
    }

    /**
     * GET  /logout : logout the current user.*
     */
    @GetMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOG.debug("REST request logout the current user");
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(config.getAuthCookieName())) {
                cookie.setValue("");
                cookie.setMaxAge(0);
                cookie.setDomain(config.getAuthCookieDomain());
                cookie.setPath(config.getAuthCookiePath());
                response.addCookie(cookie);
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.getWriter().write(config.getAuthServerUrl().substring(0,config.getAuthServerUrl().lastIndexOf("?")));
                LOG.debug("contains OAuthToken: cookie deleted, returns status 401 with return url");
            }
        }
    }
}
