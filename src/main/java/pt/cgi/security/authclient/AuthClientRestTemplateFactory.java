/*
 *  
 */
package pt.cgi.security.authclient;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pt.cgi.security.authclient.exception.AuthServerErrorHandler;
import pt.cgi.security.authclient.interceptor.AuthorizationRequestInterceptor;
import pt.cgi.security.authclient.model.OAuth2CookieModel;
import pt.cgi.security.authclient.model.UserSession;

/**
 *
 * @author Gonçalo
 */
@Component
public class AuthClientRestTemplateFactory {

    private final AuthServerErrorHandler errorHandler;
    private final AuthConfig config;
    private static final Logger LOG = LoggerFactory.getLogger(AuthClientRestTemplateFactory.class);

    private RestTemplate oAuthRestTemplateInstance = null;

    public AuthClientRestTemplateFactory(AuthServerErrorHandler errorHandler, AuthConfig config) {
        this.errorHandler = errorHandler;
        this.config = config;
    }

    public RestTemplate restTemplate(OAuth2CookieModel cookieModel) {
        RestTemplate instance = new RestTemplate();
        instance.setErrorHandler(errorHandler);
        instance.setInterceptors(Arrays.asList(new AuthorizationRequestInterceptor(cookieModel)));
        return instance;
    }

    /**
     * This one logs in with the "current user". It cannot be used with a web
     * based application ( one that relies on form login ) since the
     * Auth-provider doesn't send a clear text password for the current user
     *
     * It can be used though by a application that only wants user information
     * for it's internal consumption and don't want to user the current user
     * login
     *
     * @return
     */
    @Bean("oAuthClientRestTemplate")
    protected RestTemplate buildOAuthRestTemplate() {

        ResourceOwnerPasswordResourceDetails rDetails = new ResourceOwnerPasswordResourceDetails();
        rDetails = new ResourceOwnerPasswordResourceDetails();

        rDetails.setGrantType(config.getGrandType());
        rDetails.setAccessTokenUri(config.getAuthUrl() + "/oauth/token");
        rDetails.setClientId(config.getClientId());
        rDetails.setClientSecret(config.getClientSecret());

        rDetails.setUsername(config.getInternalUser());
        rDetails.setPassword(config.getInternalUserPassword());
        LOG.debug("using the following ResourceOwnerPasswordResourceDetails:  ");
        LOG.debug("setGrantType: {} ", config.getGrandType());
        LOG.debug("getUrl: {} ", config.getUrl() + "/oauth/token");
        LOG.debug("getClientId: {} ", config.getClientId());
        LOG.debug("getClientSecret: {} ", config.getClientSecret());
        LOG.debug("getUsername: {} ", config.getInternalUser());
        //LOG.debug("getPassword: {} ", config.getInternalUserPassword());
        OAuth2RestTemplate rTemplate = new OAuth2RestTemplate(rDetails);
        rTemplate.setErrorHandler(errorHandler);

        return rTemplate;

    }

    /**
     * Builds a RestTemplate Uses a interceptor to provide the token
     *
     * @return
     */
    public RestTemplate restTemplate() {
        if (config.isOAuthClient()) {
            if (oAuthRestTemplateInstance == null) {
                oAuthRestTemplateInstance = buildOAuthRestTemplate();
            }
            return oAuthRestTemplateInstance;
        }
        return restTemplate(getOAuth2CookieModel());
    }

    private static OAuth2CookieModel getOAuth2CookieModel() {
        UserSession session = (UserSession) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return session.getCookie();
    }

}
