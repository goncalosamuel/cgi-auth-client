package pt.cgi.security.authclient;

import org.springframework.http.HttpStatus;
import pt.cgi.security.authclient.exception.AuthServerException;
import pt.cgi.security.authclient.model.OAuth2CookieModel;
import pt.cgi.security.authclient.model.User;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;
import pt.cgi.security.authclient.model.UserSession;

/**
 * @author silvagc
 */
public class AuthClientFilter extends GenericFilterBean {

    private final AuthConfig config;
    private final AuthServerClient client;
    private static final String AUTHORIZATION_HEADER = "Authorization";

    public AuthClientFilter(AuthConfig config, AuthServerClient client) {
        this.config = config;
        this.client = client;
    }

    private static final Logger LOG = LoggerFactory.getLogger(AuthClientFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Cookie[] cookies = request.getCookies();
        int index = -1;
        Authentication auth = null;

        OAuth2CookieModel model = null;
        auth = SecurityContextHolder.getContext().getAuthentication();
                
        
        if ((index = containesOAuthToken(cookies)) > -1) {
            LOG.debug("cookie is present, we will try to use it");
            //let's try to use the cookie
            Cookie c = cookies[index];
            model = OAuth2CookieModel.buildFrom(c);
        } else {
            LOG.debug("there's no cookie, let's try the Authorization header ");
            String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);
            if (!StringUtils.isEmpty(authorizationHeader)) {
                LOG.debug("received Authorization header {} ", authorizationHeader);
                try {
                    model = OAuth2CookieModel.buildFrom(authorizationHeader);
                } catch (AuthServerException ase) {
                    LOG.warn("ase: ", ase);
                }
            }
        }
        if (model == null) {
            LOG.info("No cookie nor Authorization header were found");
        } else {
            try {
                User user = client.getCurrent(model);
                UserSession session = new UserSession(user, model);
                LOG.debug("Fecthed user: {} ", user);
                List<GrantedAuthority> authorities = new LinkedList<>();
//                if (user.getGrantedAuthorities() != null) {
//                    user.getGrantedAuthorities().stream().map(ga -> authorities.add(ga.getAuthorityId()));
//                }
                auth = new UsernamePasswordAuthenticationToken(session, null, user.getGrantedAuthorities());
            } catch (AuthServerException | org.springframework.web.client.ResourceAccessException ase) {
                LOG.warn("AuthServerException ", ase);
            }
        }

        if (auth == null) {
            LOG.debug("No auth found, sending a 401 response with redirect URL");
            String uri = request.getRequestURI();
            LOG.debug("requested uri: {} ", uri);
            String redirectUrl = config.getAuthServerUrl() + URLEncoder.encode(config
                    .getClientHomeUrl(), "UTF-8");

            if (isStaticFilePath(uri, config.getStaticFileExtensionsRegex())) {
                LOG.debug("Requested URI is a static file, therefore a redirect will be sent");
                response.sendRedirect(redirectUrl);

            } else {
                LOG.debug("Requested URI is not static file, therefore a 401 status will be used");
                LOG.debug("Doing redirect to: {} ", redirectUrl);
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.getWriter().write(redirectUrl);
            }
        } else {
            SecurityContextHolder.getContext().setAuthentication(auth);
            LOG.debug("Authentication set!");
            filterChain.doFilter(servletRequest, servletResponse);

        }
    }

    private int containesOAuthToken(Cookie[] cookies) {
        LOG.debug("containesOAuthToken: cookies {}", cookies);
        int index = -1;

        if (cookies == null) {
            return index;
        }
        for (int i = 0; i < cookies.length; i++) {
            Cookie c = cookies[i];
            LOG.warn("cookie {}", c.getName());

            if (c.getName().equals(config.getAuthCookieName())) {
                LOG.debug("containesOAuthToken: cookie found ");
                index = i;
                break;
            }
        }
        return index;
    }

    private static boolean isStaticFilePath(String uri, String staticFileExtensionRegex) {
        return uri.matches(staticFileExtensionRegex);

    }

}
