package pt.cgi.security.authclient;

import java.util.stream.Stream;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pt.cgi.security.authclient.model.UserSession;

/**
 *
 * @author silvagc
 */
@Service
public class AuthorityEvaluator {

    
    private static final Logger LOG = LoggerFactory.getLogger(AuthorityEvaluator.class);

    public boolean hasAuthority(String authorityName) {
        LOG.debug("hasAuthority: testing if current user has this authority {} ", authorityName);
        UserSession session = (UserSession) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        LOG.debug("hasAuthority result :   {} ", session.getUser().getGrantedAuthorities().stream().anyMatch(ga ->  ga.getAuthority().equals(authorityName)));
        return session.getUser().getGrantedAuthorities().stream().anyMatch(ga -> ga.getAuthority().equals(authorityName));
        
    }
    public boolean hasAnyAuthority(String... authorityName) {
        return Stream.of(authorityName).anyMatch(auth -> hasAuthority(auth));
    }

}
