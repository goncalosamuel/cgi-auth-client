package pt.cgi.security.authclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author silvagc
 */
@Component
public class AuthConfig {

    /* it's the same value yes.. */
    @Value("${pt.cgi.authserver.backend.url}")
    private String url;

    /* oauth details */
    @Value("${pt.cgi.authserver.grandType:password}")
    private String grandType;
    @Value("${pt.cgi.authserver.clientId:LOGIN_APP}")
    private String clientId;
    // raw - not encoded
    @Value("${pt.cgi.authserver.clientSecret:secret}")
    private String clientSecret;

    @Value("${pt.cgi.authserver.backend.url}")
    public String authUrl;

    @Value("${pt.cgi.authserver.cookieName:OAUTH_TOKEN}")
    private String authCookieName;
    @Value("${pt.cgi.authserver.cookieDomain:localhost}")
    private String authCookieDomain;
    @Value("${pt.cgi.authserver.cookiePath:/}")
    private String authCookiePath;
    @Value("${pt.cgi.authserver.url:http://localhost:9000/#/login?returnUrl=}")
    private String authServerUrl;
    @Value("${pt.cgi.authclient.homeUrl:http://localhost:9090}")
    private String clientHomeUrl;
    @Value("${pt.cgi.authserver.staticFileExtensionsRegex:([^\\s]+(\\.(?i)(jpg|png|gif|bmp|htm|html))$)}")
    private String staticFileExtensionsRegex;
    @Value("${pt.cgi.authserver.backend.url:http://localhost:8080}")
    private String authServerBackendUrl;
    @Value("${pt.cgi.authserver.internalUser.username:internalUser}")
    private String internalUser;
    @Value("${pt.cgi.authserver.internalUser.password:securePassword123}")
    private String internalUserPassword;

    /**
     * if set to true, the client will not use the current user in session. It
     * will use the internal user
     *
     */
    @Value("${pt.cgi.authserver.userOauthClient:false}")
    private Boolean useOAuthClient;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getGrandType() {
        return grandType;
    }

    public void setGrandType(String grandType) {
        this.grandType = grandType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getClientHomeUrl() {
        return clientHomeUrl;
    }

    public void setClientHomeUrl(String clientHomeUrl) {
        this.clientHomeUrl = clientHomeUrl;
    }

    public String getAuthCookieName() {
        return authCookieName;
    }

    public void setAuthCookieName(String authCookieName) {
        this.authCookieName = authCookieName;
    }

    public String getAuthServerUrl() {
        return authServerUrl;
    }

    public void setAuthServerUrl(String authServerUrl) {
        this.authServerUrl = authServerUrl;
    }

    public String getStaticFileExtensionsRegex() {
        return staticFileExtensionsRegex;
    }

    public void setStaticFileExtensionsRegex(String staticFileExtensionsRegex) {
        this.staticFileExtensionsRegex = staticFileExtensionsRegex;
    }

    public String getAuthServerBackendUrl() {
        return authServerBackendUrl;
    }

    public void setAuthServerBackendUrl(String authServerBackendUrl) {
        this.authServerBackendUrl = authServerBackendUrl;
    }

    public String getAuthCookieDomain() {
        return authCookieDomain;
    }

    public void setAuthCookieDomain(String authCookieDomain) {
        this.authCookieDomain = authCookieDomain;
    }

    public String getAuthCookiePath() {
        return authCookiePath;
    }

    public void setAuthCookiePath(String authCookiePath) {
        this.authCookiePath = authCookiePath;
    }

    public String getInternalUser() {
        return internalUser;
    }

    public void setInternalUser(String internalUser) {
        this.internalUser = internalUser;
    }

    public String getInternalUserPassword() {
        return internalUserPassword;
    }

    public void setInternalUserPassword(String internalUserPassword) {
        this.internalUserPassword = internalUserPassword;
    }

    public Boolean isOAuthClient() {
        return useOAuthClient;
    }

    public void setUseOAuthClient(Boolean useOAuthClient) {
        this.useOAuthClient = useOAuthClient;
    }
    

}
