package pt.cgi.security.authclient.interceptor;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import pt.cgi.security.authclient.model.OAuth2CookieModel;

import java.io.IOException;

public class AuthorizationRequestInterceptor implements ClientHttpRequestInterceptor {
    private OAuth2CookieModel model;

    private static String AUTH_HEADER_NAME = "Authorization";

    public AuthorizationRequestInterceptor(OAuth2CookieModel model) {
        this.model = model;
    }


    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] body, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        httpRequest.getHeaders().set(AUTH_HEADER_NAME, model.getTokenType() + " " + model.getAccessToken());
        return clientHttpRequestExecution.execute(httpRequest, body);
    }
}
