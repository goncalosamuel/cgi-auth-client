package pt.cgi.security.authclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

/**
 *
 * @author silvagc
 */
@Component
public class AuthClientConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private final AuthConfig config;
    private final AuthServerClient client;

    @Autowired
    public AuthClientConfigurer(AuthConfig config, AuthServerClient client) {
        this.config = config;
        this.client = client;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        AuthClientFilter customFilter = new AuthClientFilter(config, client);
        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
