package pt.cgi.security.authclient.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author silvagc
 */
public class User {

    private Long userId;

    private String login;

    private String firstName;

    private String lastName;

    private String email;

    private boolean activated = false;

    private String langKey;

    private String imageUrl;

    private String activationKey;

    private String resetKey;
    private String password;
    private Instant resetDate = null;

    private List<CustomGrantedAuthority> grantedAuthorities;
    private AuthType authTypeId;
    private Long managerId;
    private Float standardCost;

    public AuthType getAuthTypeId() {
        return authTypeId;
    }

    public void setAuthTypeId(AuthType authTypeId) {
        this.authTypeId = authTypeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public List<? extends GrantedAuthority> getAuthorities() {
        return getGrantedAuthorities();
    }

    public List<? extends GrantedAuthority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(List<CustomGrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Float getStandardCost() {
        return standardCost;
    }

    public void setStandardCost(Float standardCost) {
        this.standardCost = standardCost;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.lastName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", login=" + login + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", activated=" + activated + ", langKey=" + langKey + ", imageUrl=" + imageUrl + ", activationKey=" + activationKey + ", resetKey=" + resetKey + ", password=" + password + ", resetDate=" + resetDate + ", grantedAuthorities=" + grantedAuthorities + ", authTypeId=" + authTypeId + ", managerId=" + managerId + ", standardCost=" + standardCost + '}';
    }

    public enum AuthType {
        LDAP(0), LOCAL(1);
        int authTypeId;

        AuthType(int authTypeId) {
            System.out.println("authTypeId: " + authTypeId);
            this.authTypeId = authTypeId;
        }
    }

    /**
     * This is a workaround since Spring SimpleAuthority doesn't have a default
     * constructor in version <2
     */
    static class CustomGrantedAuthority implements GrantedAuthority {

        private String authority;

        public String getAuthority() {
            return authority;
        }

        public void setAuthority(String authority) {
            this.authority = authority;
        }

    }
}
