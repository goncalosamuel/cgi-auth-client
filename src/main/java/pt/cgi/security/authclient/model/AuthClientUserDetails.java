package pt.cgi.security.authclient.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author silvagc
 */
public class AuthClientUserDetails extends User implements UserDetails {

  
    @JsonIgnore
    @Override
    public String getPassword() {
        return "{noPassword}";
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return super.getLogin();
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return super.isActivated();
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return super.isActivated();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return super.isActivated();
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return super.isActivated();
    }

}
