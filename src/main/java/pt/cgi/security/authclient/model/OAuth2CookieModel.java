package pt.cgi.security.authclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import pt.cgi.security.authclient.exception.InvalidCookieException;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.net.URLDecoder;
import pt.cgi.security.authclient.exception.AuthServerException;

;

/**
 *
 * @author silvagc
 */
public class OAuth2CookieModel {

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("expires_in")
    private int expiresIn;
    private String scope;
    private String jti;
    private String login;

    private static final Logger LOG = LoggerFactory.getLogger(OAuth2CookieModel.class);

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public static OAuth2CookieModel buildFrom(Cookie c) throws InvalidCookieException {
        String value = c.getValue();
        OAuth2CookieModel model = null;
        try {
            String decodeCookie = URLDecoder.decode(value, "UTF-8");
            model = new ObjectMapper().readValue(decodeCookie, OAuth2CookieModel.class);
        } catch (IOException io) {
            LOG.warn("IOException: ", io);
            throw new InvalidCookieException("IOException", io);
        }
        if (StringUtils.isEmpty(model.getRefreshToken())) {
            throw new InvalidCookieException("Refresh token is required in cookie");
        }
        return model;
    }

    public static OAuth2CookieModel buildFrom(String headerValue) throws AuthServerException {
        String[] authorizationHeaderValue = headerValue.split(" ");
        OAuth2CookieModel model
                = new OAuth2CookieModel();
        if (authorizationHeaderValue.length < 2) {
            throw new AuthServerException("The Authorization header must have the token type and the token itself");
        }

        OAuth2CookieModel cookieModel = new OAuth2CookieModel();
        cookieModel.setAccessToken(authorizationHeaderValue[1]);
        cookieModel.setTokenType(authorizationHeaderValue[0]);

        return cookieModel;

    }

}
