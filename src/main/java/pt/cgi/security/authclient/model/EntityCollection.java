package pt.cgi.security.authclient.model;

public class EntityCollection<T> {
    public T[] content;

    public T[] getContent() {
        return content;
    }

    public void setContent(T[] content) {
        this.content = content;
    }
}
