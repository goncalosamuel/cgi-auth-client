package pt.cgi.security.authclient.model;

import com.fasterxml.jackson.annotation.JsonRootName;


public class AuthoritiesResponse{
    public Authority[] content;

    public Authority[] getContent() {
        return content;
    }

    public void setContent(Authority[] content) {
        this.content = content;
    }
}
