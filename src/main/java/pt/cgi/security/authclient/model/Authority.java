package pt.cgi.security.authclient.model;

/**
 *
 * @author silvagc
 */
public class Authority implements org.springframework.security.core.GrantedAuthority {

    private Long authorityId;
    private String authorityName;
    private Integer level;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Long authorityId) {
        this.authorityId = authorityId;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

    @Override
    public String getAuthority() {
        return getAuthorityName();
    }

    @Override
    public String toString() {
        return "Authority{" + "authorityId=" + authorityId + ", authorityName=" + authorityName + ", level=" + level + '}';
    }

}
