package pt.cgi.security.authclient.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 * @author silvagc
 */
public class GrantedAuthorityDeserializer extends JsonDeserializer {
    
    private static final Logger LOG = LoggerFactory.getLogger(GrantedAuthorityDeserializer.class);
    
    @Override
    public Object deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);
        String text = node.get("authority").asText();
        LOG.debug("jp: {}", text);
        LOG.debug("text: {}", text);
        if(text == null){
            return null;
        }
        return new SimpleGrantedAuthority(text);
        
    }
    
}
