package pt.cgi.security.authclient.model;

import java.util.Objects;


/**
 * @author silvagc
 */
public class GrantedAuthority {

    private Long grantedAuthorityId;
    private Authority authorityId;
    private User userId;

    public Long getGrantedAuthorityId() {
        return grantedAuthorityId;
    }

    public void setGrantedAuthorityId(Long grantedAuthorityId) {
        this.grantedAuthorityId = grantedAuthorityId;
    }


    public Authority getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Authority authorityId) {
        this.authorityId = authorityId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "GrantedAuthority{" +
                "grantedAuthorityId=" + grantedAuthorityId +
                ", authorityId=" + authorityId +
                ", userId=" + userId +
                '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.grantedAuthorityId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrantedAuthority other = (GrantedAuthority) obj;
        if (!Objects.equals(this.grantedAuthorityId, other.grantedAuthorityId)) {
            return false;
        }
        return true;
    }


}
