package pt.cgi.security.authclient.model;

public class UserSession {
    private User user;
    private OAuth2CookieModel cookie;

    public UserSession(User user, OAuth2CookieModel cookie) {
        this.user = user;
        this.cookie = cookie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OAuth2CookieModel getCookie() {
        return cookie;
    }

    public void setCookie(OAuth2CookieModel cookie) {
        this.cookie = cookie;
    }

    @Override
    public String toString() {
        return "UserSession{" + "user=" + user + ", cookie=" + cookie + '}';
    }
    
}
