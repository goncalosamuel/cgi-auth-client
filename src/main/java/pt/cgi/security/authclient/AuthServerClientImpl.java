package pt.cgi.security.authclient;

import pt.cgi.security.authclient.exception.*;
import pt.cgi.security.authclient.model.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

/**
 * @author silvagc
 */
@Component()
public class AuthServerClientImpl implements AuthServerClient {

    private final AuthConfig config;
    /* Factory must be used because we want a request based scope in here, 
            each request need to get it's own authenticatin details. 
        A Request scope bean would be an option but 
            this would break the existent clients since they all have the default ( singleton ) scope in their @component ( beans ) .
        To avoid that, the restTemplate is built in every method
     */
    private final AuthClientRestTemplateFactory restTemplateFactory;
    private static final Logger LOG = LoggerFactory.getLogger(AuthServerClientImpl.class);
    private static final String USER_PATH = "user/";
    private static final String FIND_BY_LOGIN_PATH = "user/search/findByLogin?login=";
    private static final String FIND_BY_NAME_PATH = "authority/search/findOneByAuthorityNameIgnoreCase?name=";

    @Autowired
    private AuthServerClientImpl(AuthConfig config, AuthClientRestTemplateFactory restTemplateFactory) {
        this.config = config;
        this.restTemplateFactory = restTemplateFactory;
    }

    /**
     * see interface comments
     *
     * @return
     * @throws AuthServerException
     */
    @Override
    public User getCurrent() throws AuthServerException {
        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + USER_PATH + "current", AuthClientUserDetails.class);
        return userResponse.getBody();
    }

    /**
     * see interface comments
     *
     * @deprecated since we no long use the cookie to authenticate
     * @param model
     * @return
     * @throws AuthServerException
     */
    @Override
    public User getCurrent(OAuth2CookieModel model) throws AuthServerException {

        HttpEntity<User> entity = new HttpEntity<>(createAuthorizationHeaders(model.getAccessToken()));
        LOG.debug("using header: {} ", entity.getHeaders());
        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate(model).exchange(config.getAuthServerBackendUrl() + "/" + USER_PATH + "current", HttpMethod.GET, entity, AuthClientUserDetails.class);
        return userResponse.getBody();

    }

    /**
     * see interface comments
     *
     * @param user
     * @return
     */
    @Override
    public User updateUser(User user) {
        if (user.getUserId() == null) {
            throw new InvalidParameterException("userId must not be null");
        }
        LOG.debug("Checking if the user exists so it can be updated, userId {} ", user.getUserId());
        get(user.getUserId()); //if the user doesn't exist a notfoundexception is thrown
        if (user.getGrantedAuthorities() != null) {
            LOG.warn("createUser method doesn't save the user authorities. Use the setAuthorities() or grantAuthority method for that purpose!!");
            LOG.warn("Cleaning user grantedAuthorities");
            user.setGrantedAuthorities(null);
        }
        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate().postForEntity(config.getAuthServerBackendUrl() + "/" + USER_PATH, user, AuthClientUserDetails.class);
        return userResponse.getBody();
    }

    /**
     * see interface comments
     *
     * @param user
     * @return
     * @throws AuthServerException
     */
    @Override
    public User createUser(User user) throws AuthServerException {
        if (user.getUserId() != null) {
            throw new InvalidParameterException("userId must be null");
        }
        LOG.debug("Checking if the user doesn't exists so it can be created, userId {} ", user.getLogin());
        try {
            getUserByUsername(user.getLogin());
            throw new ConflitException("user with this login already exists");
        } catch (NotFoundException nf) {
            //keep going
        }

        if (user.getGrantedAuthorities() != null) {
            LOG.warn("createUser method doesn't save the user authorities. Use the setAuthorities() or grantAuthority method for that purpose!!");
            LOG.warn("Cleaning user grantedAuthorities");
            user.setGrantedAuthorities(null);
        }
        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate().postForEntity(config.getAuthServerBackendUrl() + "/" + USER_PATH, user, AuthClientUserDetails.class);
        return userResponse.getBody();
    }

    /**
     * see interface comments
     *
     * @param userId
     * @return
     */
    @Override
    public User get(Long userId) {
        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + USER_PATH + userId, AuthClientUserDetails.class);
        return userResponse.getBody();
    }

    /**
     * see interface comments
     *
     * @param username
     * @return
     * @throws AuthServerException
     */
    @Override
    public User getUserByUsername(String username) throws AuthServerException {
        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + FIND_BY_LOGIN_PATH + username, AuthClientUserDetails.class);

        return userResponse.getBody();
    }

    /**
     * see interface comments
     *
     * @param userId
     */
    @Override
    public void disableUser(Long userId) {
        User user = get(userId);
        user.setActivated(false);
        updateUser(user);
    }

    /**
     * see interface comments
     *
     * @param login
     * @param passwordHash encrypted using Bcrypt
     * @param email
     * @param language
     * @param firstName
     * @param lastName
     * @return
     * @throws AuthServerException
     */
    @Override
    public User createExternalUser(String login, String passwordHash, String email, String language, String firstName, String lastName) throws AuthServerException {
        User user = new User();
        user.setLogin(login);
        user.setPassword(passwordHash);
        user.setEmail(email);
        user.setLangKey(language);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setActivated(true);
        user.setAuthTypeId(User.AuthType.LOCAL);

        ResponseEntity<AuthClientUserDetails> userResponse = restTemplateFactory.restTemplate().postForEntity(config.getAuthServerBackendUrl() + "/" + USER_PATH, user, AuthClientUserDetails.class);
        return userResponse.getBody();
    }

    @Override
    public Authority getAuthority(String name) throws AuthServerException {

        ResponseEntity<Authority> authorityResponse = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + FIND_BY_NAME_PATH + name, Authority.class);

        return authorityResponse.getBody();
    }

    /**
     * AUTHORITIES
     */
    private static final String AUTHORITY_URI = "authority/";
    private static final String AUTHORITY_GET_URI = "authority/{authorityId}";
    private static final String AUTHORITY_SEARCH_BY_USER_URI = AUTHORITY_URI + "search/findAuthoritiesByUserId?userId={userId}";
    private static final String AUTHORITY_SEARCH_BY_USER_AND_ID_URI = AUTHORITY_URI + "search/getUserAuthority?authorityId={authorityId}&userId={userId}";

    /**
     * see interface comments
     *
     * @param authorityId
     * @return
     * @throws AuthServerException
     */
    @Override
    public Authority getAuthority(Long authorityId) throws AuthServerException {
        ResponseEntity<Authority> response = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + AUTHORITY_GET_URI,
                Authority.class,
                authorityId);
        return response.getBody();

    }

    /**
     * see interface comments
     *
     * @param userId
     * @return
     * @throws AuthServerException
     */
    @Override
    public Collection<Authority> getAuthorities(Long userId) throws AuthServerException {
        ResponseEntity<AuthoritiesResponse> response = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + AUTHORITY_SEARCH_BY_USER_URI,
                AuthoritiesResponse.class,
                userId);

        return Arrays.asList(response.getBody().getContent());

    }

    /**
     * see interface comments
     *
     * @param userId
     * @param authorityId
     * @return
     * @throws AuthServerException
     */
    @Override
    public boolean hasAuthority(Long userId, Long authorityId) throws AuthServerException {
        LOG.debug("hasAuthority called for user {} and for authority", userId, authorityId);
        try {
            ResponseEntity<Authority> response = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + AUTHORITY_SEARCH_BY_USER_AND_ID_URI,
                    Authority.class,
                    authorityId, userId);
            LOG.debug("hasAuthority status: {} ", response.getStatusCode());
            LOG.debug("hasAuthority body: {} ", response.getBody());
            return response.getBody() != null;
        } catch (NotFoundException notFound) {
            LOG.debug("user {} doesn't have authority {}", userId, authorityId);
            return false;
        }

    }

    /**
     * GRANTED AUTHORITIES
     */
    private static final String GRANTED_AUTHORITY_URI = "grantedAuthority/";
    private static final String GRANTED_AUTHORITY_ID_URI = "grantedAuthority/{grantedAuthorityId}";
    private static final String GRANTED_AUTHORITY_SEARCH_BY_USER_AUTH_URI = "grantedAuthority/search/getByUserAndAuthority?userId={userId}&authorityId={authorityId}";

    /**
     * see interface comments
     *
     * @param userId
     * @param authorityId
     * @return
     * @throws AuthServerException
     */
    @Override
    public GrantedAuthority grantAuthority(Long userId, Long authorityId) throws AuthServerException {
        if (hasAuthority(userId, authorityId)) {
            LOG.debug("User {} already has authority {}", userId, authorityId);
            throw new ConflitException();
        }
        return restTemplateFactory.restTemplate().postForEntity(config.getAuthServerBackendUrl() + "/" + GRANTED_AUTHORITY_URI, buildGrantedAuthoritiesMap(userId, authorityId), GrantedAuthority.class).getBody();
    }

    /**
     * see interface comments
     *
     * @param userId
     * @param authorityId
     */
    @Override
    public void revokeAuthority(Long userId, Long authorityId) {
        LOG.debug("revokeAuthority called with userId {} and authorityId {}", userId, authorityId);
        ResponseEntity<GrantedAuthority> gaResponse = restTemplateFactory.restTemplate().getForEntity(config.getAuthServerBackendUrl() + "/" + GRANTED_AUTHORITY_SEARCH_BY_USER_AUTH_URI,
                GrantedAuthority.class, userId, authorityId);
        LOG.debug("revokeAuthorityResult {} ", gaResponse.getBody());
        GrantedAuthority ga = gaResponse.getBody();
        restTemplateFactory.restTemplate().delete(config.getAuthServerBackendUrl() + GRANTED_AUTHORITY_ID_URI, ga.getGrantedAuthorityId());
    }

    /**
     * See interface comments
     *
     * @param userId
     * @param authorityId
     */
    @Override
    public void setAuthorities(Long userId, Collection<Long> authorityIds) {
        LOG.debug("setAuthorities called with userId {} and authorityIds {}", userId, authorityIds);
        ResponseEntity<User> userWithAuthorities
                = restTemplateFactory.restTemplate().postForEntity(config.getAuthServerBackendUrl() + "/" + USER_PATH + userId + "/" + AUTHORITY_URI, authorityIds, User.class);
        LOG.debug("setAuthoritiesResult {} ", userWithAuthorities.getBody());

    }

    /**
     * Util METHODS
     *
     */
    /**
     * This just exists so we can create a granted authority and the association
     * between authority and user
     */
    private static String AUTHORITY_ID_NAME = "authorityId";
    private static String USER_ID_NAME = "userId";

    private Map<String, String> buildGrantedAuthoritiesMap(Long userId, Long authorityId) {
        Map<String, String> map = new HashMap<>();

        map.put(AUTHORITY_ID_NAME, config.getAuthServerBackendUrl() + AUTHORITY_URI + authorityId);
        map.put(USER_ID_NAME, config.getAuthServerBackendUrl() + USER_PATH + userId);
        return map;

    }



    private static HttpHeaders createAuthorizationHeaders(String refreshToken) {
        return new HttpHeaders() {
            {
                String authHeader = "Bearer " + refreshToken;
                set("Authorization", authHeader);
            }
        };
    }



}
