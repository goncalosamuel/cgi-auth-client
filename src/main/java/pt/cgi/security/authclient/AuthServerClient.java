package pt.cgi.security.authclient;

import pt.cgi.security.authclient.exception.AuthServerException;
import pt.cgi.security.authclient.model.OAuth2CookieModel;
import pt.cgi.security.authclient.model.User;

import java.util.Collection;
import pt.cgi.security.authclient.model.Authority;
import pt.cgi.security.authclient.model.GrantedAuthority;

/**
 *
 * @author silvagc
 */
public interface AuthServerClient {

    /**
     * Uses the auth-token provided in the oauth2Model
     *
     * @param oauth2Model
     * @return
     * @throws AuthServerException
     */
    public User getCurrent(OAuth2CookieModel oauth2Model) throws AuthServerException;

    /**
     * Uses the "current" auth-token, meaning that with will get the UserSession
     * from springContext
     *
     * @return
     * @throws AuthServerException
     */
     public User getCurrent() throws AuthServerException;

    /**
     * Updates user data Granted authorities are included for convinience the
     * User POJO but they are not persisted Use grantAuthority or
     * revokeAuthority to add/remove authorities
     *
     * @param user
     * @return
     * @throws AuthServerException
     */
    public User updateUser(User user) throws AuthServerException;

    /**
     * Creates a new User Granted authorities are included for convinience the
     * User POJO but they are not persisted Use grantAuthority or
     * revokeAuthority to add/remove authorities
     *
     * @param user
     * @return
     * @throws AuthServerException
     */
    public User createUser(User user) throws AuthServerException;

    /**
     * Must find an User by it's ID
     *
     * @param userId
     * @return
     * @throws AuthServerException
     * @throws pt.cgi.security.authclient.exception.NotFoundException when the
     * uses doesn't exist in the system
     */
    public User get(Long userId) throws AuthServerException;

    /**
     * Find a certain user by login
     *
     * @param username
     * @return
     * @throws AuthServerException
     * @throws pt.cgi.security.authclient.exception.NotFoundException when the
     * uses doesn't exist in the system
     */
    public User getUserByUsername(String username) throws AuthServerException;

    /**
     * Set the user as inactive
     *
     * @param userId
     * @throws AuthServerException
     */
    public void disableUser(Long userId) throws AuthServerException;

    /**
     * Creates an user that has the AuthType = LOCAL
     *
     * @param login
     * @param passwordHash encrypted using Bcrypt
     * @param email
     * @param language
     * @param firstName
     * @param lastName
     * @return
     * @throws AuthServerException
     */
    public User createExternalUser(String login, String passwordHash, String email, String language, String firstName, String lastName) throws AuthServerException;

    /**
     * Authorities *
     */
    public Authority getAuthority(String name) throws AuthServerException;

    public Authority getAuthority(Long authorityId) throws AuthServerException;

    public Collection<Authority> getAuthorities(Long userId) throws AuthServerException;

    public boolean hasAuthority(Long userId, Long authorityId) throws AuthServerException;

    /**
     * Granted Authorities *
     */
    public GrantedAuthority grantAuthority(Long userId, Long authorityId);

    public void revokeAuthority(Long userId, Long authorityId);

    /**
     * Removes all the existant authorities and grants the authorities with the
     * given ids. The user can only create users with the same or lower
     * authority level
     *
     * @param userId
     * @param authorityId
     * @
     */
    public void setAuthorities(Long userId, Collection<Long> authorityId);

}
