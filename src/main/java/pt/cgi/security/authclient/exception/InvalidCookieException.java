package pt.cgi.security.authclient.exception;

/**
 *
 * @author silvagc
 */
public class InvalidCookieException extends AuthServerException{

    public InvalidCookieException() {
    }

    public InvalidCookieException(String message) {
        super(message);
    }

    public InvalidCookieException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCookieException(Throwable cause) {
        super(cause);
    }
    
    
}
