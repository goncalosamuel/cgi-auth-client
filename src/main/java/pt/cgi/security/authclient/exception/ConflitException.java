package pt.cgi.security.authclient.exception;

public class ConflitException extends AuthServerException {
    public ConflitException() {
        super();
    }

    public ConflitException(String message) {
        super(message);
    }

    public ConflitException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflitException(Throwable cause) {
        super(cause);
    }
}
