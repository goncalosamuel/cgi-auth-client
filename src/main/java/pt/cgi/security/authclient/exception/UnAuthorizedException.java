package pt.cgi.security.authclient.exception;

/**
 *
 * @author silvagc
 */
public class UnAuthorizedException extends AuthServerException{

    public UnAuthorizedException() {
    }

    public UnAuthorizedException(String message) {
        super(message);
    }

    public UnAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnAuthorizedException(Throwable cause) {
        super(cause);
    }
    
}
